function keys(obj) {

    let keysArray = [];

    // if(typeof obj === 'string'){
    //     return keysArray; // handles the case if string is passed 
    // }

    // iterating over object to get keys
    for(let key in obj){
        keysArray.push(key); // pushing keys to array
    }
    
    return keysArray;
}

module.exports = keys;
