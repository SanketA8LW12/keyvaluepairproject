// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

function invert(obj) {
    let objInvert = {};
    for(let keys in obj){
        if(typeof obj[keys] !== 'function'){ // ignores if type is function
            objInvert[obj[keys]] = keys;
        }
    }
    return objInvert;
}

module.exports = invert;