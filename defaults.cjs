function defaults(obj, defaultProps) {
    for (let key in defaultProps) {
        // handling the case if key is present but null or undefined
        if ((key in obj) === false || obj[key] === null || obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    }
}

module.exports = defaults;



