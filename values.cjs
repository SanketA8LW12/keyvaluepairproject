function values(obj) {

    let valueArray = [];

    for(let property in obj){
        
        if(typeof obj[property] !== 'function'){ // ignores if type is function
            valueArray.push(obj[property]); // adding values to array
        }

    }

    return valueArray;
}



module.exports = values;