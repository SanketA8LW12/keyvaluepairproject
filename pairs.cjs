function pairs(obj) {
    let pairArray = [];
    for(let keys in obj){

        if(typeof obj[keys] !== 'function'){ // ignores if type is function
            let tempPairArr = [];
            tempPairArr.push(keys);
            tempPairArr.push(obj[keys]);
            pairArray.push(tempPairArr);
        }
        
    }
    return pairArray;
}

module.exports = pairs;