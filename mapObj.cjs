function mapObject(obj, cb) {
    for(let key in obj){
        if(typeof obj[key] !== 'function'){ // ignores if type is function
            obj[key] = cb(obj[key]);
        }
        
    }
    return obj;
}
module.exports = mapObject;